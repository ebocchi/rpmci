# Changelog

Please take note of the following changes, they may require changes to your `.gitlab-ci.yml` files:

### 2022-12-06
 - 5107f5c: Building for AlmaLinux 8/9 and RHEL 8/9 is now supported.

### 2022-06-16
 - c440c84: Build multi-arch docker images, supporting `x86_64` and `aarch64`.

### 2022-01-03
 - 2e12254c: [Dynamic BuildRequires](https://fedoraproject.org/wiki/Changes/DynamicBuildRequires) are now supported on CS9.

### 2021-12-01
 - 30d3dcec: Advertised support for CS9 by adding it to the README.

### 2021-09-15
 - be4939e: Gitlab's `rules` have several unintended side effects and unresolved bugs, so we're rolling back to `only/except`.

### 2021-09-14
 - 2ee9eff: Migrating from `only/except` to [`rules`](https://docs.gitlab.com/ee/ci/yaml/#rules). If you wish
   to modify the existing rules you will have to copy the original ones, as Gitlab CI does not merge them correctly.
   Note that `only/except` and `rules` can't coexist, so if you have `only/except` directives now, you will need
   to convert them.

### 2021-08-06
 - 581a0732: Add a new stage `docker_test`.

### 2021-03-01
 - 61beeba3: Advertised support for CS8 by adding it to the README.

### 2021-02-22
 - fd271dd9: Added [convenience functions](README.md#convenience-functions) to simplify common operations.

### 2021-01-12
 - f8440982: The `builder-slc6` image is no longer updated. The existing image is still available and support for
    SLC6 will not be dropped from RPMCI for the time being.

### 2020-11-13
 - e1da6daa: `srpm` and `rpm` dependencies can now be [specified per OS](README.md#adding-build-dependencies).

### 2020-10-16
 - f60ad792: RPMCI will now automatically [add a `VCS` tag](README.md#vcs-tag-in-spec-files) to your spec files, allowing you
   to trace a built RPM back to the source code that was used to build it.

### 2020-06-08
 - 280b660: RPMCI can now build multiple source RPMs. Your `Makefile` will need to be adapted to create multiple source RPMs
   and place them in `build/SRPMS/`.

### 2020-03-03
 - de3c839: The `builder-cc7` image no longer includes the repos for Software Collections. If you wish to use them, you will need
   to add a `yum install -y centos-release-scl` to your gitlab jobs.

### 2020-02-17
 - f388493: `test_rpmlint` was renamed to `.test_rpmlint`.

### 2020-01-28
 - c925722: Changed default value of `BUILD_7` to `False`. Current users may have to adjust their configuration if they relied on the default.
