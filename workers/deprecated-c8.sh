COLORS="\e[41m\e[1m\e[97m"
RESET="\e[0m"

echo "  "
echo "  "
echo -e "    ${COLORS}****************************************************************${RESET}"
echo -e "    ${COLORS}**               /!\ End of support for CL8 /!\               **${RESET}"
echo -e "    ${COLORS}**                                                            **${RESET}"
echo -e "    ${COLORS}** As of January 1st 2022, CentOS Linux 8 will no longer be   **${RESET}"
echo -e "    ${COLORS}** supported. Please stop using this image.                   **${RESET}"
echo -e "    ${COLORS}**                                                            **${RESET}"
echo -e "    ${COLORS}**                  More info in OTG0065266.                  **${RESET}"
echo -e "    ${COLORS}**                                                            **${RESET}"
echo -e "    ${COLORS}****************************************************************${RESET}"
echo "  "
echo "  "

unset BASH_ENV
