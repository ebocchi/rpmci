COLORS="\e[41m\e[1m\e[97m"
RESET="\e[0m"

echo "  "
echo "  "
echo -e "    ${COLORS}****************************************************************${RESET}"
echo -e "    ${COLORS}**               /!\ End of support for SLC6 /!\              **${RESET}"
echo -e "    ${COLORS}**                                                            **${RESET}"
echo -e "    ${COLORS}** As of December 1st 2020, SLC6 will no longer be supported. **${RESET}"
echo -e "    ${COLORS}** Please stop using this image. More info in OTG0054345.     **${RESET}"
echo -e "    ${COLORS}**                                                            **${RESET}"
echo -e "    ${COLORS}****************************************************************${RESET}"
echo "  "
echo "  "

unset BASH_ENV
