DOCKER_SLC6		= Dockerfile.slc6
DOCKER_CC7		= Dockerfile.cc7
DOCKER_C8		= Dockerfile.c8
DOCKER_CS8		= Dockerfile.cs8
DOCKER_EL8		= Dockerfile.el8
DOCKER_AL8		= Dockerfile.al8
DOCKER_CS9		= Dockerfile.cs9
DOCKER_EL9		= Dockerfile.el9
DOCKER_AL9		= Dockerfile.al9
IMAGE_SLC6		= builder-slc6
IMAGE_CC7		= builder-cc7
IMAGE_C8		= builder-c8
IMAGE_CS8		= builder-cs8
IMAGE_EL8		= builder-el8
IMAGE_AL8		= builder-al8
IMAGE_CS9		= builder-cs9
IMAGE_EL9		= builder-el9
IMAGE_AL9		= builder-al9
CPP_OPTIONS     = -nostdinc -undef -fdirectives-only


all: $(DOCKER_CC7) $(DOCKER_C8) $(DOCKER_CS8) $(DOCKER_EL8) $(DOCKER_AL8) $(DOCKER_CS9) $(DOCKER_EL9) $(DOCKER_AL9)

clean:
	rm -rf $(DOCKER_SLC6) $(DOCKER_CC7) $(DOCKER_C8) $(DOCKER_CS8) $(DOCKER_EL8) $(DOCKER_AL8) $(DOCKER_CS9) $(DOCKER_EL9) $(DOCKER_AL9)

$(DOCKER_SLC6): EXTRA_PACKAGES = rpmlint yum-plugin-priorities
$(DOCKER_SLC6): $(DOCKER_SLC6).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_SLC6) $(DOCKER_SLC6).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_SLC6)

$(DOCKER_CC7): EXTRA_PACKAGES = rpmlint yum-plugin-priorities
$(DOCKER_CC7): $(DOCKER_CC7).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_CC7) $(DOCKER_CC7).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_CC7)

$(DOCKER_C8): EXTRA_PACKAGES = rpmlint epel-release 'dnf-command(config-manager)'
$(DOCKER_C8): $(DOCKER_C8).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_C8) $(DOCKER_C8).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_C8)

$(DOCKER_CS8): EXTRA_PACKAGES = rpmlint epel-release epel-next-release 'dnf-command(config-manager)'
$(DOCKER_CS8): $(DOCKER_CS8).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_CS8) $(DOCKER_CS8).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_CS8)

$(DOCKER_EL8): EXTRA_PACKAGES = rpmlint epel-release 'dnf-command(config-manager)'
$(DOCKER_EL8): $(DOCKER_EL8).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_EL8) $(DOCKER_EL8).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_EL8)

$(DOCKER_AL8): EXTRA_PACKAGES = rpmlint epel-release 'dnf-command(config-manager)'
$(DOCKER_AL8): $(DOCKER_AL8).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_AL8) $(DOCKER_AL8).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_AL8)

$(DOCKER_CS9): EXTRA_PACKAGES = rpmlint epel-release epel-next-release 'dnf-command(config-manager)'
$(DOCKER_CS9): $(DOCKER_CS9).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_CS9) $(DOCKER_CS9).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_CS9)

$(DOCKER_EL9): EXTRA_PACKAGES = rpmlint epel-release 'dnf-command(config-manager)'
$(DOCKER_EL9): $(DOCKER_EL9).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_EL9) $(DOCKER_EL9).in
	sed -i "s|EXTRA_PACKAGES|$(EXTRA_PACKAGES)|g" $(DOCKER_EL9)

$(DOCKER_AL9): EXTRA_PACKAGES = rpmlint epel-release 'dnf-command(config-manager)'
$(DOCKER_AL9): $(DOCKER_AL9).in *.docker
	cpp $(CPP_OPTIONS) -o $(DOCKER_AL9) $(DOCKER_AL9).in
	sed -i "s/EXTRA_PACKAGES/$(EXTRA_PACKAGES)/g" $(DOCKER_AL9)

build_$(DOCKER_SLC6): $(DOCKER_SLC6)
	docker build --rm --build-arg DEFAULT_OS=6 -t $(IMAGE_SLC6) -f $(DOCKER_SLC6) .

build_$(DOCKER_CC7): $(DOCKER_CC7)
	docker build --rm --build-arg DEFAULT_OS=7 -t $(IMAGE_CC7) -f $(DOCKER_CC7) .

build_$(DOCKER_C8): $(DOCKER_C8)
	docker build --rm --build-arg DEFAULT_OS=8 -t $(IMAGE_C8) -f $(DOCKER_C8) .

build_$(DOCKER_CS8): $(DOCKER_CS8)
	docker build --rm --build-arg DEFAULT_OS=8s -t $(IMAGE_CS8) -f $(DOCKER_CS8) .

build_$(DOCKER_EL8): $(DOCKER_EL8)
	docker build --rm --build-arg DEFAULT_OS=8el -t $(IMAGE_EL8) -f $(DOCKER_EL8) .

build_$(DOCKER_AL8): $(DOCKER_AL8)
	docker build --rm --build-arg DEFAULT_OS=8al -t $(IMAGE_AL8) -f $(DOCKER_AL8) .

build_$(DOCKER_CS9): $(DOCKER_CS9)
	docker build --rm --build-arg DEFAULT_OS=9 -t $(IMAGE_CS9) -f $(DOCKER_CS9) .

build_$(DOCKER_EL9): $(DOCKER_EL9)
	docker build --rm --build-arg DEFAULT_OS=9el -t $(IMAGE_EL9) -f $(DOCKER_EL9) .

build_$(DOCKER_AL9): $(DOCKER_AL9)
	docker build --rm --build-arg DEFAULT_OS=9al -t $(IMAGE_AL9) -f $(DOCKER_AL9) .

build: build_$(DOCKER_CC7) build_$(DOCKER_C8) build_$(DOCKER_CS8) build_$(DOCKER_EL8) build_$(DOCKER_AL8) build_$(DOCKER_CS9) build_$(DOCKER_EL9) build_$(DOCKER_AL9)
